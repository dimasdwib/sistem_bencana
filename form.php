<div>
<div class="card">
<div class="card-body">
<div class="row">
  <div class="col">
    <div class="bar">
      <ul>
        <!-- <li><a href="#"> Form </a></li>
        <li><a href="#"> Setting </a></li> -->
        <li><a href="?act=logout"> Logout </a></li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">

    <div class="card">
      <div class="card-body form-left">
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Jenis Bencana</label>
          <div class="col-sm-8">
            <select name="bencana" class="form-control">
              <option value="0"> -- Pilih Bencana -- </option>
            </select>    
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Lokasi Kejadian</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="lokasi" placeholder="Lokasi Kejadian"></textarea>    
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label"></label>
          <div class="col-sm-8">
            <div class="row">
              <div class="col">
                Provinsi
                <select name="provinsi" class="form-control">
                  <option value="0"> -- Pilih Provinsi -- </option>
                </select>   
              </div>
              <div class="col">
                Kota/Kabupaten
                <select name="kabupaten" class="form-control">
                  <option value="0"> -- Pilih Kota/Kab -- </option>
                </select>  
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label"></label>
          <div class="col-sm-8">
            <div class="row">
              <div class="col">
                Kecamatan
                <select name="kecamatan" class="form-control">
                  <option value="0"> -- Pilih Kecamatan -- </option>
                </select>   
              </div>
              <div class="col">
                Kelurahan/Desa
                <select name="desa" class="form-control">
                  <option value="0"> -- Pilih Kel/Desa -- </option>
                </select>  
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Waktu Kejadian</label>
          <div class="col-sm-8">
            <div class="input-append date form_datetime">
              <input class="form-control" name="waktu" placeholder="Waktu Kejadian">
              <span class="add-on"><i class="icon-calendar"></i></span>
            </div>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Korban</label>
          <div class="col-md-8">
            <div class="row">
              <div class="col">
                <input type="checkbox" name="korban" data-toggle="toggle" data-on="<i class='fa fa-frown-open fa-fw'></i> Ada" data-off="<i class='fa fa-fw fa-grin-beam'></i> Tidak Ada" data-onstyle="danger" data-offstyle="success" >
              </div>
            </div>
            <div class="row" id="korban_input" style="display:none;">
              <div class="col">
                <label> Meninggal</label>
                <input type="number" name="korban_inp_m" class="form-control" value="0" min="0" />
              </div>
              <div class="col">
                <label> Luka Ringan</label>
                <input type="number" name="korban_inp_r" class="form-control" value="0" min="0" />
              </div>
              <div class="col">
                <label> Luka Berat</label>
                <input type="number" name="korban_inp_b" class="form-control" value="0" min="0" />
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Pengungsi</label>
          <div class="col-md-8">
            <div class="row">
              <div class="col">
                <input type="checkbox" name="pengungsi" data-toggle="toggle" data-on="<i class='fa fa-frown-open fa-fw'></i> Ada" data-off="<i class='fa fa-fw fa-grin-beam'></i> Tidak Ada" data-onstyle="danger" data-offstyle="success" />
              </div>
            </div>
            <div class="row" id="pengungsi_input" style="display:none;">
              <div class="col">
                <label> KK</label>
                <input type="number" class="form-control" name="pengungsi_inp_kk" value="0" min="0" />
              </div>
              <div class="col">
                <label> Jiwa</label>
                <input type="number" class="form-control" name="pengungsi_inp_jiwa" value="0" min="0" />
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Ancaman</label>
          <div class="col-md-8">
            <div class="row">
              <div class="col">
                <input type="checkbox" name="ancaman" data-toggle="toggle" data-on="<i class='fa fa-frown-open fa-fw'></i> Ada" data-off="<i class='fa fa-fw fa-grin-beam'></i> Tidak Ada" data-onstyle="danger" data-offstyle="success" />
              </div>
            </div>
            <div class="row" id="ancaman_input" style="display:none;">
              <div class="col">
                <label> KK</label>
                <input type="number" class="form-control" value="0" name="ancaman_inp_kk" min="0" />
              </div>
              <div class="col">
                <label> Jiwa </label>
                <input type="number" class="form-control" value="0" name="ancaman_inp_jiwa" min="0" />
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Rumah Rusak</label>
          <div class="col-md-8">
            <div class="row">
              <div class="col">
                <input type="checkbox" name="rumah" data-toggle="toggle" data-on="<i class='fa fa-frown-open fa-fw'></i> Ada" data-off="<i class='fa fa-fw fa-grin-beam'></i> Tidak Ada" data-onstyle="danger" data-offstyle="success" />
              </div>
            </div>
            <div class="row" id="rumah_input" style="display:none;">
              <div class="col">
              <label>Roboh</label>
                <input type="number" class="form-control" value="0" min="0" name="rumah_inp_ro" />
              </div>
              <div class="col">
                <label> Rusak</label>
                <input type="number" class="form-control" value="0" min="0" name="rumah_inp_ru" />
              </div>
            </div> 
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-4 col-form-label">Gambar</label>
          <div class="col-sm-8">
            <input type="file" >
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <h3 class="text-center"> Preview </h3>
        <p id="preview"></p>
        <input type="hidden" name="preview_input">
        <div id="button" style="display: none">
          <a target="_blank" href="" id="cetak" class="btn btn-primary"><i class="fa fa-print fa-fw"></i> Cetak </a>
          &nbsp;
          <button class="btn btn-primary" id="simpan"><i class="fa fa-save fa-fw"></i> Simpan </button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

</div>