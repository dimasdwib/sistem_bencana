<?php

/*
 * Config
 * App Config
 * 
*/

return [
  'config' => [
    /* Config for wordpress database */
    'dbwordpress' => [
      'database' => 'dbwordpress',
      'host'     => 'localhost',
      'prefix'   => 'wp_',
      'username' => 'root',
      'password' => '',
    ],
    /* Config for app database */
    'dbapp' => [
      'database' => 'kbptn',
      'host'     => 'localhost',
      'username' => 'root',
      'password' => '',
    ],
  ],
];