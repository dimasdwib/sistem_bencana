<?php
session_start();
include 'config.php';
$page = isset($_GET['page']) ? $_GET['page'] : 'form';
if (!isset($_SESSION['user'])) {
  // must be login fisrt
  $page = 'login';
} else {
  // user loged in
  if ($page == 'login') {
    $page='form';
  }
}

if (isset($_GET['act'])) {
  if ($_GET['act'] == 'logout') {
    session_destroy();
    $page = 'login';
    header('location: ?page=login');
  }
}
?>
<!doctype html>
<html>
<head>
  <title> Sistem Bencana </title>
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-grid.min.css" type="text/css" />
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css" type="text/css" />
  <link rel="stylesheet" href="style.css" type="text/css" />
  <link rel="stylesheet" href="plugin/datepicker/css/bootstrap-datetimepicker.min.css" type="text/css" />
  <link rel="stylesheet" href="plugin/select2/select2-bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="plugin/select2/select2.min.css" type="text/css" />

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
  
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
  <script src="plugin/datepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="plugin/select2/select2.min.js"></script>
</head>
<body>
<div class="container-fluid">
<?php 
switch($page) {
  case 'login' :
    include 'login.php';
    break;
  case 'form' :
    include 'form.php';
    break;
  case 'cetak' :
    include 'cetak.php';
    break;
  default :
    echo 'Undefined';
}
?>
<script src="functions.js" type="text/javascript"></script>
</div>
</body>
</html>