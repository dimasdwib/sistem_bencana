<?php
  $err = null;
  if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($username == 'admin' && $password == 'admin') {
      $_SESSION['user'] = [
        'name' => 'Admin',
      ];
      header("location: ?page=form");
    } else {
      $err = 'Username/password salah';
    }
  }
?>
<div class="row">
  <div class="col">
    <div class="card login-card">
      <div class="card-body">
        <form method="post">
        <div class="text-center">
          <h3 class="text-center"><i class="fa fa-lock fa-fw"></i> Login </h3>
          <small class="text-danger"> <?= $err ?></small>
        </div>
        <hr />
        <div class="form-group">
          <label> Username </label>
          <input type="text" name="username" class="form-control" />
        </div>
        <div class="form-group">
          <label> Password </label>
          <input type="password" name="password" class="form-control" />
        </div>
        <div class="form-group">
          <button class="btn btn-primary"> Submit </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>