<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-body">
        <table class="table table-striped">
          <tr>
            <td style="width:180px">Bencana</td>
            <td><?= $_GET['bencana'] ?></td>
          </tr>
          <tr>
            <td>Lokasi</td>
            <td><?= $_GET['lokasi'] ?></td>
          </tr>
          <tr>
            <td>Provinsi</td>
            <td><?= $_GET['provinsi'] ?></td>
          </tr>
          <tr>
            <td>Kota/Kabupaten</td>
            <td><?= $_GET['kabupaten'] ?></td>
          </tr>
          <tr>
            <td>Kecamatan</td>
            <td><?= $_GET['kecamatan'] ?></td>
          </tr>
          <tr>
            <td>Kelurahan/Desa</td>
            <td><?= $_GET['desa'] ?></td>
          </tr>
          <tr>
            <td>Waktu Kejadian</td>
            <td><?= $_GET['hari'] ?> <?= $_GET['tanggal'] ?> <?= $_GET['pukul'] ?> WIB</td>
          </tr>
          <tr>
            <td>Korban Meninggal</td>
            <td><?= $_GET['km'] ?></td>
          </tr>
          <tr>
            <td>Korban Luka Berat</td>
            <td><?= $_GET['kr'] ?></td>
          </tr>
          <tr>
            <td>Korban Luka Berat</td>
            <td><?= $_GET['kb'] ?></td>
          </tr>
          <tr>
            <td>Pengungs KK</td>
            <td><?= $_GET['pk'] ?></td>
          </tr>
          <tr>
            <td>Pengungs Jiwa</td>
            <td><?= $_GET['pj'] ?></td>
          </tr>
          <tr>
            <td>Ancaman KK</td>
            <td><?= $_GET['ak'] ?></td>
          </tr>
          <tr>
            <td>Ancaman Jiwa</td>
            <td><?= $_GET['aj'] ?></td>
          </tr>
          <tr>
            <td>Rumah Rusak Ringan</td>
            <td><?= $_GET['rr'] ?></td>
          </tr>
          <tr>
            <td>Rumah Rusak Berat</td>
            <td><?= $_GET['ro'] ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.print();
  });
</script>