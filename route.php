<?php
/*
 * App Routing
 * Control app request
 * 
*/
include 'config.php';
include 'siquery/siquery.php';

class app_request {

  function __construct() {
    $config = include 'config.php';
    $this->config = $config;
    $this->wp = new siquery($config['config']['dbwordpress']);
    $this->db = new siquery($config['config']['dbapp']);
  }

  function get_bencana() {
    $bencana = $this->db->from('jenisben')
                    ->orderBy('nama ASC')
                    ->get();
    if ($bencana) {
      echo json_encode($bencana);
    }
  }

  function get_provinsi() {
    $data = $this->db->from('provinsi')
                    ->orderBy('nama ASC')
                    ->get();
    if ($data) {
      echo json_encode($data);
    }
  }

  function get_kabupaten() {
    $id_prov = $_GET['id_prov'];
    $data = $this->db->from('kabupaten')
                    ->where('id_prov', $id_prov)
                    ->orderBy('nama ASC')
                    ->get();
    if ($data) {
      echo json_encode($data);
    }
  }

  function get_kecamatan() {
    $id_kab = $_GET['id_kab'];
    $data = $this->db->from('kecamatan')
                    ->where('id_kabupaten', $id_kab)
                    ->orderBy('nama ASC')
                    ->get();
    if ($data) {
      echo json_encode($data);
    }
  }

  function get_desa() {
    $id_kec = $_GET['id_kec'];
    $data = $this->db->from('desa')
                    ->where('id_kecamatan', $id_kec)
                    ->orderBy('nama ASC')
                    ->get();
    if ($data) {
      echo json_encode($data);
    }
  }

  function simpan() {
    /* INsert to wordpress database */
    $post = $_POST['post'];
    $prefix = $this->config['config']['dbwordpress']['prefix'];
    $qtime = date('Y-m-d H:i:s');

    $admin = $this->wp->from($prefix.'users')
                  ->where('user_login', 'admin')
                  ->first();

    $judul = "Telah terjadi bencana";

    $insert = $this->wp->insert($prefix.'posts', [
      'post_date' => $qtime,
      'post_date_gmt' => $qtime,
      'post_author' => $admin->ID,
      'post_content' => $post,
      'post_title' => $judul,
      'post_name' => $judul,
      'post_modified' => $qtime,
      'post_modified_gmt' => $qtime,
    ]);

    if ($insert->execute()) {
      echo 'success';
    } else {
      echo 'failed';
    }
  }

}

/* Exec Request */
$request_post = isset($_POST['act']) ? $_POST['act'] : null;
$request_get = isset($_GET['req']) ? $_GET['req'] : null;
if ($request_post != null) {
  $app = new app_request();
  $app->{$request_post}();
} else {
  $app = new app_request();
  $app->{$request_get}();
}