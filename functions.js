/*
 * JS File
*/

/* Preview */
function update_preview() {
  var bencana = $("[name='bencana'] option:selected").text();
  var provinsi = $("[name='provinsi'] option:selected").text();
  var kabupaten = $("[name='kabupaten'] option:selected").text();
  var kecamatan = $("[name='kecamatan'] option:selected").text();
  var desa = $("[name='desa'] option:selected").text();
  var lokasi = $("[name='lokasi']").val();
  var waktu = $("[name='waktu']").val();

  var korban = $("[name='korban']").prop('checked');
  var korban_inp_m = Number($("[name='korban_inp_m']").val());
  var korban_inp_r = Number($("[name='korban_inp_r']").val());
  var korban_inp_b = Number($("[name='korban_inp_b']").val());

  var pengungsi = $("[name='pengungsi']").val();
  var pengungsi_kk = Number($("[name='pengungsi_inp_kk']").val());
  var pengungsi_jiwa = Number($("[name='pengungsi_inp_jiwa']").val());

  var ancaman = $("[name='ancaman']").prop('checked');
  var ancaman_kk = Number($("[name='ancaman_inp_kk']").val());
  var ancaman_jiwa = Number($("[name='ancaman_inp_jiwa']").val());

  var rumah = $("[name='rumah']").prop('checked');
  var rumah_ro = Number($("[name='rumah_inp_ro']").val());
  var rumah_ru = Number($("[name='rumah_inp_ru']").val());

  /* Split waktu */
  waktu = waktu.split(" ");
  var tanggal = waktu[0];
  var pukul = waktu[1];
  var date = new Date(tanggal);
  var hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];
  var preview = "Telah terjadi "+bencana+
                " di "+lokasi+
                ", kelurahan "+desa+
                ", kecamatan "+kecamatan+
                ", "+kabupaten+
                ", provinsi "+provinsi+
                " pada "+hari[date.getDay()]+
                " tanggal "+tanggal+
                " pukul "+pukul+
                " WIB.";

  if (korban) {
    if (korban_inp_b > 0 && korban_inp_m > 0 && korban_inp_r > 0) {
      preview += " Terdapat "+korban_inp_m+" korban meninggal, "+korban_inp_r+" orang luka ringan, "+korban_inp_b+" orang luka berat,";
    } else if (korban_inp_m > 0 && korban_inp_r > 0) {
      preview += " Terdapat "+korban_inp_b+" korban meninggal dan "+korban_inp_r+" luka ringan ,";
    } else if (korban_inp_m > 0 && korban_inp_b > 0) {
      preview += " Terdapat "+korban_inp_m+" korban meninggal dan "+korban_inp_b+" luka berat ,";
    } else if (korban_inp_b > 0 && korban_inp_r > 0) {
      preview += " Terdapat "+korban_inp_b+" orang luka berat dan "+korban_inp_r+" luka ringan ,";
    } else if (korban_inp_m > 0) {
      preview += " Terdapat kurang lebih "+korban_inp_m+" korban meninggal, ";
    } else if (korban_inp_b > 0) {
      preview += " Terdapat kurang lebih "+korban_inp_b+" korban luka berat, ";
    } else if (korban_inp_r > 0) {
      preview += " Terdapat kurang lebih "+korban_inp_r+" korban luka ringan ,";
    } else {
      preview += " Belum ada laporan mengenai korban jiwa dalam bencana tersebut baik korban meninggal maupun yang terluka,";
    }
  } else {
    preview += " Belum ada laporan mengenai korban jiwa dalam bencana tersebut baik korban meninggal maupun yang terluka,";
  }

  if (pengungsi) {
    if (pengungsi_kk > 0 && pengungsi_jiwa > 0) {
      preview += " dan dari data yang telah dilaporkan, jumlah pengungsi kk sampai saaat ini diperkirakan sebanyak "+pengungsi_kk+" KK serta pengungsi per-jiwa sebanyak "+pengungsi_jiwa+" orang.";
    } else if (pengungsi_jiwa > 0) {
      preview += " dan dari data yang telah dilaporkan, jumlah pengungsi jiwa sampai saat ini diperkirakan mencapai "+pengungsi_jiwa+" orang.";
    } else if (pengungsi_kk > 0) {
      preview += " dan dari data yang telah dilaporkan, jumlah pengungsi kk sampai saat ini diperkirakan hingga "+pengungsi_kk+" kk.";
    } else {
      preview += " dan dari data yang telah dilaporkan, masih belum ada jumlah pengungsi hingga saat ini.";
    }
  } else {
    preview += " dan dari data yang telah dilaporkan, masih belum ada jumlah pengungsi hingga saat ini.";
  }

  if (ancaman) {
    if (ancaman_kk > 0) {
      preview += " Ancaman kk ada sekitar "+ancaman_kk+" kk,";
    } else {
      preview += " Ancaman terhadap kk belum terdata,";
    } 
    
    if (ancaman_jiwa > 0) {
      preview += " ancaman jiwa sebanyak "+ancaman_jiwa+" orang.";
    } else {
      preview += " ancaman terhadap jiwa belum dilaporkan.";
    }
  }

  if (rumah) {
    if (rumah_ro > 0 && rumah_ru > 0) {
      preview += " Kejadian tersebut menngakibatkan sedikitnya, "+rumah_ro+" rumah rusak berat dan "+rumah_ru+" rumah rusak ringan.";      
    } else if (rumah_ru > 0) {
      preview += " Kejadian tersebut menngakibatkan sedikitnya, "+rumah_ru+" rumah rusak ringan.";
    } else if (rumah_ro > 0) {
      preview += " Kejadian tersebut menngakibatkan sedikitnya, "+rumah_ro+" rumah rusak berat.";
    } else {
      preview += " Tidak ada rumah rusak dalam kejadian tersebut.";
    }
  } else {
    preview += " Tidak ada rumah rusak dalam kejadian tersebut.";
  }

  $("#preview").html(preview);
  $("[name='preview_input']").val(preview);
  $("#button").show();

  var link = "?page=cetak";
  link += '&bencana='+bencana;
  link += '&provinsi='+provinsi;
  link += '&kabupaten='+kabupaten;
  link += '&lokasi='+lokasi;
  link += '&kecamatan='+kecamatan;
  link += '&desa='+desa;
  link += '&provinsi='+provinsi;
  link += '&hari='+hari[date.getDay()];
  link += '&tanggal='+tanggal;
  link += '&pukul='+pukul;
  
  link += '&km='+korban_inp_m;
  link += '&kr='+korban_inp_r;
  link += '&kb='+korban_inp_b;

  link += '&pk='+pengungsi_kk;
  link += '&pj='+pengungsi_jiwa;

  link += '&ak='+ancaman_kk;
  link += '&aj='+ancaman_jiwa;

  link += '&rr='+rumah_ru;
  link += '&ro='+rumah_ro;

  $("#cetak").attr("href", link);
}

/* All Ajax Request */
function get_bencana() {
  $.ajax({
    url: 'route.php?req=get_bencana',
  })
  .done(function(data) {
    var bencana = JSON.parse(data);
    $.each(bencana, function(i, d) {
      $("[name='bencana']").append('<option>'+ d.nama +'</option>');
    });
  })
  .fail(function(err) {
    console.log(err);
  });
}

function get_provinsi() {
  $.ajax({
    url: 'route.php?req=get_provinsi',
    beforeSend: function() {
      $("[name='provinsi']").attr('disabled', true);
      $("[name='kabupaten']").attr('disabled', true);
      $("[name='kecamatan']").attr('disabled', true);    
      $("[name='desa']").attr('disabled', true); 
    }
  })
  .done(function(data) {
    var res = JSON.parse(data);
    $("[name='provinsi']").removeAttr('disabled');
    $.each(res, function(i, d) {
      $("[name='provinsi']").append('<option value="'+d.id+'">'+ d.nama +'</option>');
    });
  })
  .fail(function(err) {
    console.log(err);
  });
}

function get_kabupaten(id_prov) {
  if (id_prov == 0) {
    return fasle;
  }
  $.ajax({
    url: 'route.php?req=get_kabupaten&id_prov='+id_prov,
    beforeSend: function() {
      $("[name='kecamatan']").attr('disabled', true);
      $("[name='desa']").attr('disabled', true);   
    }
  })
  .done(function(data) {
    var res = JSON.parse(data);
    $("[name='kabupaten']").html('<option value="0">-- Pilih Kota/Kab. --</option>');
    $("[name='kabupaten']").removeAttr('disabled');
    
    $.each(res, function(i, d) {
      $("[name='kabupaten']").append('<option value="'+d.id+'">'+ d.nama +'</option>');
    });
  })
  .fail(function(err) {
    console.log(err);
  });
}

function get_kecamatan(id_kab) {
  if (id_kab == 0) {
    return fasle;
  }
  $.ajax({
    url: 'route.php?req=get_kecamatan&id_kab='+id_kab,
    beforeSend: function() {
      $("[name='desa']").attr('disabled', true);
    }
  })
  .done(function(data) {
    var res = JSON.parse(data);
    $("[name='kecamatan']").html('<option value="0">-- Pilih Kecamatan --</option>');
    $("[name='kecamatan']").removeAttr('disabled');
    $("[name='desa']").val(0);
    $.each(res, function(i, d) {
      $("[name='kecamatan']").append('<option value="'+d.id+'">'+ d.nama +'</option>');
    });
  })
  .fail(function(err) {
    console.log(err);
  });
}

function get_desa(id_kec) {
  if (id_kec == 0) {
    return fasle;
  }
  $.ajax({
    url: 'route.php?req=get_desa&id_kec='+id_kec,
    beforeSend: function() {
      $("[name='desa']").attr('disabled', true);
    }
  })
  .done(function(data) {
    var res = JSON.parse(data);
    $("[name='desa']").html('<option value="0">-- Pilih Kel/Desa --</option>');
    $("[name='desa']").removeAttr('disabled');
    $.each(res, function(i, d) {
      $("[name='desa']").append('<option value="'+d.id+'">'+ d.nama +'</option>');
    });
  })
  .fail(function(err) {
    console.log(err);
  });
}

function simpan() {
  var data = $("[name='preview_input']").val();
  $.ajax({
    url: 'route.php',
    method: 'post',
    data: { post: data, act: 'simpan' },
    beforeSend: function() {
      $("#simpan").attr('disabled', true);
      $("#simpan").text("Loading ..");
    }
  })
  .done(function(data) {
    if (data == 'success') {
      alert('Data berhasil disimpan');
      window.location.reload();
    } else {
      alert('Data gagal disimpan');
      $("#simpan").removeAttr('disabled');
      $("#simpan").text("Simpan");
    }
  })
  .fail(function(err) {
    console.log(err);
    alert('Data gagal disimpan');
    $("#simpan").removeAttr('disabled');
    $("#simpan").text("Simpan");
  });
}

/* Init Request */
$(document).ready(function() {

  get_bencana();
  get_provinsi();
  update_preview();

  $("[name='waktu']").datetimepicker({format: 'dd-mm-yyyy hh:ii'});
  $("[name='bencana'], [name='provinsi'], [name='kabupaten'], [name='kecamatan'], [name='desa']").select2({ theme: 'bootstrap' });

  /* Handle All Input */
  $("input, select").change(function() {
    update_preview();
  });
  $("[name='lokasi']").keyup(function() {
    update_preview();
  })


  /* Handle Provinsi */
  $("[name='provinsi']").on('change', function() {
    get_kabupaten($(this).val());
  });

  /* Handle Kabupaten/Kota */
  $("[name='kabupaten']").on('change', function() {
    get_kecamatan($(this).val());
  });

  /* Handle Kecamatan */
  $("[name='kecamatan']").on('change', function() {
    get_desa($(this).val());
  });

  /* Handle Korban */
  $("[name='korban'").change(function() {
    if ($(this).prop('checked')) {
      $("#korban_input").show();
    } else {
      $("#korban_input").hide();
    }
  });

   /* Handle Pengungsi */
   $("[name='pengungsi'").change(function() {
    if ($(this).prop('checked')) {
      $("#pengungsi_input").show();
    } else {
      $("#pengungsi_input").hide();
    }
  });

   /* Handle Ancaman */
   $("[name='ancaman'").change(function() {
    if ($(this).prop('checked')) {
      $("#ancaman_input").show();
    } else {
      $("#ancaman_input").hide();
    }
  });

   /* Handle Rumah */
  $("[name='rumah'").change(function() {
    if ($(this).prop('checked')) {
      $("#rumah_input").show();
    } else {
      $("#rumah_input").hide();
    }
  });

  $("#simpan").click(function() {
    simpan();
  });

});